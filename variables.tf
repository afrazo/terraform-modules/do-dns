variable "domain" {
  description = "Domain we'll be adding records for"
}

variable "main_ip" {
  description = ""
  default     = null
}

variable "a_records" {
  description = "List of maps with 'name' and 'value' keys for A records"
  default     = []
}

variable "cname_records" {
  description = "List of maps with 'name' and 'value' keys for CNAME records"
  default     = []
}

variable "txt_records" {
  description = "List of maps with 'name' and 'value' keys for TXT records"
  default     = []
}

variable "mx_records" {
  description = "List of maps with 'name' and 'value' keys for MX records"
  default     = []
}

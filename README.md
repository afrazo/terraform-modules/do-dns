Manage DNS on Digital Ocean.

## Usage

```hcl
module "example_com" {
  source = "git::ssh://git@gitlab.com/afrazo/terraform-modules/do-dns?ref=1.0.0"

  domain  = "example.com"
  main_ip = 1.2.3.4

  a_records = [
    { name = "@", value = "2.3.4.5" },
    { name = "www", value = "2.3.4.5" },
    { name = "staging", value = "1.2.3.4" }
  ]

  txt_records = [
    { name = "@", value = "v=spf1 ip4:2.3.4.5 mx include:_spf.google.com ~all" },
    { name = "@", value = "something else" }
  ]

  mx_records = [
    { name = "10", value = "2.3.4.5" },
    { name = "20", value = "2.3.4.6" }
  ]
}
```

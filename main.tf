resource "digitalocean_domain" "this_domain" {
  name       = var.domain
  ip_address = var.main_ip
}

resource "digitalocean_record" "this_a_record" {
  for_each = { for a_record in var.a_records : "${a_record.name}=${a_record.value}" => a_record }

  domain = var.domain
  type   = "A"
  name   = each.value.name
  value  = each.value.value
  ttl    = 180
}

resource "digitalocean_record" "this_cname_record" {
  for_each = { for cname_record in var.cname_records : "${cname_record.name}=${cname_record.value}" => cname_record }

  domain = var.domain
  type   = "CNAME"
  name   = each.value.name
  value  = each.value.value
  ttl    = 180
}

resource "digitalocean_record" "this_txt_record" {
  for_each = { for txt_record in var.txt_records : "${txt_record.name}=${txt_record.value}" => txt_record }

  domain = var.domain
  type   = "TXT"
  name   = each.value.name
  value  = each.value.value
  ttl    = 180
}

resource "digitalocean_record" "this_mx_record" {
  for_each = { for mx_record in var.mx_records : "${mx_record.name}=${mx_record.value}" => mx_record }

  domain   = var.domain
  type     = "MX"
  name     = "@"
  ttl      = 180
  priority = each.value.name
  value    = each.value.value
}
